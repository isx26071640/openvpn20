#! /bin/bash
openssl req -new -x509 -nodes -keyout cakey.pem -out cacert.pem
openssl genrsa -out serverkey.pem 2048 #genera clau privada
openssl req -new -key serverkey.pem -out serverreq.pem #genera request 
openssl x509 -CAkey cakey.pem -CA cacert.pem -req -in serverreq.pem -extfile ext.alternate.conf -out servercert.pem -CAcreateserial #signa request amb CA
#SERVER
sudo openvpn --remote [IP remote] --dev tun1 --ifconfig 10.4.0.1 10.4.0.2 --tls-server --dh dh2048.pem --ca cacert.pem --cert servercert.pem --key serverkey.pem --reneg-sec 60 --verb 2
#CLIENT
openvpn --remote [IP remote] --dev tun1 --ifconfig 10.4.0.2 10.4.0.1 --tls-client --ca cacert.pem --cert clientcert.pem --key clientkey.pem --reneg-sec 60 --verb 2
